const express = require('express');

const app = express();
const port = process.env.PORT || 8001;
app.use(express.json())

const { book } = require('./models');
const { todo } = require('./models');
const { user } = require('./models');

// fungsi books ===============================================
app.get('/books', (req, res) => {
    book.findAll().then(books => {
      res.status(200).json({
        status: true,
        message: 'Books retrieved!',
        data: { books }
      })
    })
  })

  app.get('/books/:id', (req, res) => {
    book.findByPk(req.params.id).then(books => {
      res.status(200).json({
        status: true,
        message: `Books with ID ${req.params.id} retrieved!`,
        data: { books }
      })
    })
  })

  app.post('/books', (req, res) => {
    book.create({
      title: req.body.title,
      author: req.body.author
    }).then(book => {
      res.status(201).json({
        status: true,
        message: 'Books created!',
        data: book 
      })
    })
  })

  app.put('/books/:id', (req, res) => {
    Book.findByPk(req.params.id).then(book => {
      book.update({
        title: req.body.title,
        author: req.body.author
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `Books with ID ${req.params.id} updated!`,
          data: book 
        })
      })
    })
  })
  
  app.delete('/books/:id', (req, res) => {
    Book.destroy({
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(204).end()
    })
  })
  
// fungsi todos ===============================================
 
  app.get('/todos', async (req, res) => {
  try {
  const data = await todo.findAll({
    order: [
      ['id', 'ASC']
  ]
  });
    res.status(200).json({
      status: true,
      message: `todos retrieved!`,
      data
    })
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.get('/todos/:id', async (req, res) => {
    try {
    const data = await todo.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `todos with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      res.status(200).json({
        status: true,
        message: `todos with ID ${req.params.id} retrieved!`,
        data
      })
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })
  

  app.post('/todos', async (req, res) => {
  try {
  const findIdUser = await user.findOne
  
  if (findIdUser === null) {
    res.status(201).json({
      status: false,
      message: 'Can not insert todos because user id not found!',
    })
  } else {
    todo.create({
      name: req.body.name,
      description: req.body.description,
      due_at :req.body.due_at,
      user_id: req.body.user_id
    }).then(todo => {
      res.status(201).json({
        status: true,
        message: 'todos created!',
        data: todo 
      })  
    })
    }   
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.delete('/todos/:id', async (req, res) => {
    try {
    let n = await todo.destroy({
      where: {
        id: req.params.id
      }
    });
    if (n == 0) {
      res.status(202).json({
        status: false,
        message: `todos with id ${req.params.id} not found!`
      })
    }
    else {
      res.status(200).json({
        status: true,
        message: `${n} rows executed!! todos with id ${req.params.id} deleted!`
      })
    }
   }
   catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })



  app.put('/todos/:id', async (req, res) => {
    try { 
     const data = await todo.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `todos with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      const findIdUser = await user.findOne({ where: { id: req.body.user_id } });
      if (findIdUser === null) {
        res.status(201).json({
        status: false,
        message: 'Can not update todos because user id not found!',
        })
      } else { 
      let dataupdate = await todo.update({
        name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
        description: (req.body.description == null || req.body.description == "") ? data.description : req.body.description,
        due_at: (req.body.password == null || req.body.password == "") ? data.due_at : req.body.due_at,
        user_id:(req.body.user_id == null || req.body.user_id == "") ? data.user_id : req.body.user_id
      }, {
        where: {
          id: req.params.id
        }
      })
      const datanew = await todo.findByPk(req.params.id);
      if (dataupdate == 1 && datanew ) {
        res.status(200).json({
        status: true,
        message: `${dataupdate} rows executed!! todos with ID ${req.params.id} updated!`,
        data,
        datanew
      })
      }
      }
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })

// fungsi users ===============================================
  
 app.get('/users', async (req, res) => {
  try {
  const data = await user.findAll({
    order: [
      ['id', 'ASC']
  ]
  });
    res.status(200).json({
      status: true,
      message: `users retrieved!`,
      data
    })
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.get('/users/:id', async (req, res) => {
  try {
   const data = await user.findByPk(req.params.id);
   if (data === null) {
    res.status(202).json({
      status: false,
      message: `users with ID ${req.params.id} not Found!`,
      data
    })
   } 
   else {
    res.status(200).json({
      status: true,
      message: `users with ID ${req.params.id} retrieved!`,
      data
    })
   } 
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.post('/users', async (req, res) => {
    try {
     const [data, created] = await user.findOrCreate({
          where: {email: req.body.email}, 
          defaults: {
                name: req.body.name,
                email: req.body.email,
                password: (req.body.password == null || req.body.password == "") ?'tsel1234' : req.body.password                    
                    }
        });
        if (created) {
          res.status(201).json({
            status: true,
            message: 'user created!',
            data
          })
        }
        else {
          res.status(202).json({
            status: false,
            message: 'email already exist!'
          })
        }
      }
    catch(err) {
        res.status(422).json({
          status: false,
          message: err.message
        })
      }
      })   


  app.delete('/users/:id', async (req, res) => {
  try { 
    let n = await user.destroy({
      where: {
        id: req.params.id
      }
    });
    if (n == 0) {
      res.status(202).json({
        status: false,
        message: `user with id ${req.params.id} not found!`
      })
    }
    else {
      res.status(200).json({
        status: true,
        message: `${n} rows executed!! user with id ${req.params.id} deleted!`
      })
    }
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })
  

  app.put('/users/:id', async (req, res) => {
    try {
     const data = await user.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `users with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      let dataupdate = await user.update({
        name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
        email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
        password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password
      }, {
        where: {
          id: req.params.id
        }
      })
      const datanew = await user.findByPk(req.params.id);
      if (dataupdate == 1 && datanew ) {
      res.status(200).json({
        status: true,
        message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
        data,
        datanew
      })
      }
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })



app.listen(port, () => { console.log(` app listening at http://localhost:${port}`)});