const { Book, Users, Todos } = require('../models')

function getAllUser(req, res, next){
    try{
        Users.findAll().then(user => {
            res.status(200).json({
                status: true,
                message: 'User retreived',
                user
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function getUserById(req, res, next){
    try{
        Users.findByPk(req.params.id).then(user => {
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function updateUser(req, res, next){
   try {
    Users.findByPk(req.params.id).then(user => {
        if (user === null) {
            res.status(200).json({
                status: false,
                message: `User ID ${req.params.id} not found`,
                user
            })
        } else {
            Users.update({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            }, {
                where: {
                    id: req.params.id
                }
            }).then(user => {
                res.status(200).json({
                    status: true,
                    message: `User with ID ${req.params.id} updated!`,
                    user
                })
            }) 
        }
    })
}
catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
}

function deleteOneUser(req, res, next){
    try{
        Users.findOne({ where: {id: req.params.id}}).then(user => {
            if (user === null){
                res.status(201).json({
                    status: false,
                    message: `User ID ${req.params.id} does not exist`
                })
            } else {
                Users.destroy({ where: {id: req.params.id}}).then(user => {
                    res.status(200).json({
                        status: true,
                        message: `User ID ${req.params.id} deleted`
                    })
                })
            }
           })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
}

module.exports = {getAllUser, getUserById, updateUser, deleteOneUser}