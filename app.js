const express = require('express')
const app = express()
const port = 3000
const findIndex = id => books.findIndex(i => i.id == id);

app.use(express.json());

const books = [{
  id:1,
  title: 'Sapiens',
  author: 'Noah'
}]

const handleNotFound = (req, res) => {
  res.status(404).json({
    status: false,
    message: `Book with ID ${req.params.id} not found!`,
  })
} 

app.get('/books', (req, res) => {
  res.status(200).json({
    books
  })
})

//app.get('/books', (req, res) => {
//  res.status(200).json({
//    books
//  })
//})
app.post('/books', (req, res) => {
  const book = {
    id: (books[books.length - 1].id || 0) + 1,
    title: req.body.title,
    author: req.body.author
  }
  books.push(book)
  res.status(201).json({
    book
  })
})

// GET /books/1
app.get('/books/:id', (req, res) => {
  const book = books.find((i) => i.id == req.params.id)
  res.status(200).json({
    book
  })
})

app.put('/books/:id', (req, res) => {
  // Cari index buku dulu
  // Terus replace dengan request body, tapi jangan replace id
  const book = Book.update(req.params.id, req.body)
  if (!book) return handleNotFound(req, res);
  res.status(200).json({
    status: true,
    message: `Book with ID ${req.params.id} updated!`,
    data: book
  })
})

app.delete('/books/:id', (req, res) => {
  // Diimplementasi sendiri.
  const bookIndex = findIndex(req.params.id)
  if (bookIndex < 0) return false;
  /* Menghapus buku dengan index x
   * di dalam array books.
   * */
  books.splice(bookIndex, 1)
  //const book = Book.delete(req.params.id)
  //if (!book) return handleNotFound(req, res);
  res.status(204).end()

})




//app.post('/books', (req, res) => {
//  const book = {
//    title: req.body.title,
//    author: req.body.author
//  }
//  books.push(book)
//  res.status(201).json({
//    book
//  })
//})

app.get('/', (req, res) => {
  res.json({
    message: 'Hello world'
  })
})

app.get('/about', (req, res) => {
  res.json({
    message: 'Test about'
  })
  })

app.post('/sum', (req,res) => {
  //console.log(req.body.x)
  const result = req.body.x + req.body.y
  res.json({
    parameters: {
      x: req.body.x,
      y: req.body.y
    },
    operation: 'sum',
    result
  })
}) 

app.put('/sum2', (req,res) => {
  //console.log(req.body.x)
  const result = req.body.x + req.body.y
  res.json({
    parameters: {
      x: req.body.x,
      y: req.body.y
    },
    operation: 'sum',
    result
  })
}) 

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})