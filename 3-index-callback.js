const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const userController = require('./controller/userController')
const { Book, Users, Todos } = require('./models')

app.use(express.json())

app.get('/users', function(req, res) {
    try{
        userController.getAllUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.get('/users/:id', function(req, res){
    try{
        userController.getUserById(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.put('/users/:id', function(req, res){
    try{
        userController.updateUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.delete('/users/:id', function(req, res){
    try{
        userController.deleteOneUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.listen(port, () => console.log(`Listening on port ${port}!`))