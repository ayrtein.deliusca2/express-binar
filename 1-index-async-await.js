const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const { Book, Users, Todos } = require('./models')

app.use(express.json())

//books 
app.get('/books', (req, res) => {
  Book.findAll().then(books => {
    res.status(200).json({
      status: true,
      message: 'Books retrieved!',
      data: { books }
    })
  })
})

app.get('/books/:id', (req, res) => {
  Book.findByPk(req.params.id).then(book => {
    res.status(200).json({
      status: true,
      message: `Books with ID ${req.params.id} retrieved!`,
      data: book
    })
  })
})

app.post('/books', (req, res) => {
  Book.create({
    title: req.body.title,
    author: req.body.author
  }).then(book => {
    res.status(201).json({
      status: true,
      message: 'Books created!',
      data: book 
    })
  })
})


app.put('/books/:id', (req, res) => {
  Book.findByPk(req.params.id).then(book => {
    book.update({
      title: req.body.title,
      author: req.body.author
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Books with ID ${req.params.id} updated!`,
        data: book 
      })
    })
  })
})

app.delete('/books/:id', (req, res) => {
  Book.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//users =======================

app.get('/users', async(req,res) => {
  try {
    const user = await Users.findAll()
    res.status(200).json({
      status: true,
      message: 'User retrieved!',
      user
    })
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})

app.get('/users/:id', async(req, res) => {
  try{
    const user = await Users.findByPk(req.params.id)
    if (user === null){
      res.status(202).json({
        status: false,
        message: `User ID ${req.params.id} does not exist` 
      })
    } else {
      res.status(200).json({
        status: true,
        message: `User ID ${req.params.id} retrieved`,
        user
      })
    }
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})

app.post('/users', async(req, res) => {
  try{
    const [user, created] = await Users.findOrCreate({
      where: {email: req.body.email},
      defaults: {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      }
    })
    if (created){
      res.status(201).json({
        status: true,
        message: `New user created with ID ${req.body.id}`,
        user
      })
    } else {
      res.status(202).json({
        status: false,
        message: `Create user failed, email ${req.body.email} already exist`
      })
    }
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})

app.put('/users/:id', async(req, res) => {
  try{
    const user = await Users.findByPk(req.params.id)
    if (user === null){
      res.status(202).json({
        status: false,
        message: `User ID ${req.params.id} not found`,
        user
      })
    } else {
      let userTrue = await Users.update({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      }, {
        where: {
          id: req.params.id
        }
      })
      const userUpdated = await Users.findByPk(req.params.id)
      if (userTrue){
        res.status(200).json({
          status: true,
          message: `User ID ${req.params.id} updated!`,
          userUpdated
      })
      }
    }
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
})

app.delete('/users/:id', async(req, res) => {
  try{
    let selectedUserId = await Users.destroy({
      where: {
        id: req.params.id
      }
    })
    if (selectedUserId === 0) {
      res.status(202).json({
        status: false,
        message: `User ID ${req.params.id} does not exist`
      })
    } else {
      res.status(200).json({
        status: true,
        message: `User ID ${req.params.id} deleted`
      })
    }
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})


//todos ======================

app.get('/todos', async (req, res) => {
  try {
    const todo = await Todos.findAll()
    res.status(200).json({
      status: true,
      message: 'User retrieved!',
      todo
    })
  }
  catch(err) {
    res.status(442).json({
      status: false,
      message: err.message
    })
  }
  })

app.get('/todos/:id', async (req, res) => {
  try{
    const todo = await Todos.findByPk(req.params.id)
    if (todo === null) {
      res.status(202).json({
        status: false,
        message: `Todo ID ${req.params.id} not found`,
        todo
      })
    } else {
      res.status(200).json({
        status: true,
        message: `Todo ID ${req.params.id} retreived`,
        todo
    })
  }
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})
    

app.post('/todos', async (req, res) => {
  try {
    const selectUserId = await Users.findOne({ where: { id: req.body.user_id } });
    if (selectUserId === null){
      res.status(201).json({
        status: false,
        message: `Create todo is failed due to user ID ${req.body.user_id} not found `
      })
    } else {
      Todos.create({
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id:req.body.user_id
    }).then(todos => {
      res.status(201).json({
        status: true,
        message: 'todos created!',
        data: todos 
      })
    })
  }
} 
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})

app.put('/todos/:id', async (req, res) => {
  try {
    const todo = await Todos.findByPk(req.params.id)
    if (todo === null){
      res.status(202).json({
        status: false,
        message: `Todo ID ${req.params.id} not found`,
        todo
      })
    } else {
    const selectUserId = await Users.findOne({ where: { id: req.body.user_id } })
    if (selectUserId === null){
      res.status(201).json({
        status: false,
        message: `Update todo is failed due to user ID ${req.body.user_id} not found `
      })
    } else {
      let todoTrue = await Todos.update({
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id:req.body.user_id
      }, {
        where: {
          id: req.params.id
        }
      })
      const todoUpdated = await Todos.findByPk(req.params.id)
      if (todoTrue){
        res.status(200).json({
          status: true,
          message: `Todos with ID ${req.params.id} updated!`,
          todoUpdated
      })
        }
      }
    }
    }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})


app.delete('/todos/:id', async(req, res) => {
  try{
    let selectedTodoId = await Todos.destroy({
      where: {
        id: req.params.id
      }
    })
    if (selectedTodoId === 0) {
      res.status(202).json({
        status: false,
        message: `Todo ID ${req.params.id} does not exist`
      })
    } else {
      res.status(200).json({
        status: true,
        message: `Todo ID ${req.params.id} deleted`
      })
    }
  }
  catch(err){
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
})


app.listen(port, () => console.log(`Listening on port ${port}!`))
