const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// const { Book, User } = require('./models')
const { Book, Users, Todos } = require('./models')

app.use(express.json())
//books 
app.get('/books', (req, res) => {
  Book.findAll().then(books => {
    res.status(200).json({
      status: true,
      message: 'Books retrieved!',
      data: { books }
    })
  })
})

app.get('/books/:id', (req, res) => {
  Book.findByPk(req.params.id).then(book => {
    res.status(200).json({
      status: true,
      message: `Books with ID ${req.params.id} retrieved!`,
      data: book
    })
  })
})

app.post('/books', (req, res) => {
  Book.create({
    title: req.body.title,
    author: req.body.author
  }).then(book => {
    res.status(201).json({
      status: true,
      message: 'Books created!',
      data: book 
    })
  })
})


app.put('/books/:id', (req, res) => {
  Book.findByPk(req.params.id).then(book => {
    book.update({
      title: req.body.title,
      author: req.body.author
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Books with ID ${req.params.id} updated!`,
        data: book 
      })
    })
  })
})

app.delete('/books/:id', (req, res) => {
  Book.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//users
app.get('/users', (req, res) => {
  Users.findAll().then(users => {
    res.status(200).json({
      status: true,
      message: 'User retrieved!',
      data: { users }
    })
  })
})

app.get('/users/:id', (req, res) => {
  Users.findByPk(req.params.id).then(users => {
    res.status(200).json({
      status: true,
      message: `User with ID ${req.params.id} retrieved!`,
      data: users
    })
  })
})

app.post('/users', (req, res) => {
  Users.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }).then(user => {
    res.status(201).json({
      status: true,
      message: 'User created!',
      data: user 
    })
  })
})


app.put('/users/:id', (req, res) => {
  Users.findByPk(req.params.id).then(users => {
    Users.update({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `User with ID ${req.params.id} updated!`,
        data: users 
      })
    })
  })
})

app.delete('/users/:id', (req, res) => {
  Users.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//todos ======================

app.get('/todos', async (req, res) => {
  try {
    const data = await Todos.findAll()
    res.status(200).json({
      status: true,
      message: 'User retrieved!',
      data
    })
  }
  catch(err) {
    res.status(442).json({
      status: false,
      message: err.message
    })
  }
  })

app.get('/todos/:id', (req, res) => {
  Todos.findByPk(req.params.id).then(todos => {
    res.status(200).json({
      status: true,
      message: `Todo with ID ${req.params.id} retrieved!`,
      data: todos
    })
  })
})

app.post('/todos', (req, res) => {
  Todos.create({
    name: req.body.name,
    description: req.body.description,
    due_at: req.body.due_at,
    user_id:req.body.user_id
  }).then(todos => {
    res.status(201).json({
      status: true,
      message: 'todos created!',
      data: todos 
    })
  })
})


app.put('/todos/:id', (req, res) => {
  Todos.findByPk(req.params.id).then(todos => {
    Todos.update({
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id:req.body.user_id
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Todos with ID ${req.params.id} updated!`,
        data: todos 
      })
    })
  })
})

app.delete('/todos/:id', (req, res) => {
  Todos.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
