const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const { Book, Users, Todos } = require('./models')

app.use(express.json())

//users ==================================

app.get('/users', (req, res) => {
    try{
        Users.findAll().then(user => {
            res.status(200).json({
                message:true,
                message: 'User retreived',
                user
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
})

app.get('/users/:id', (req, res) => {
    try{
        Users.findByPk(req.params.id).then(user => {
            if (user === null){
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user    
                }) 
            } else {
                res.status(200).json({
                    status: true,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user    
                })
            }
          
        })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
        })
      }
})

app.post('/users', (req, res) => {
    try{
        Users.findOne({ where: {email: req.body.email}}).then(user => {
            if (user){
                res.status(201).json({
                    status: false,
                    message: `Create user failed due to ${req.body.email} already exist`
                })
            } else {
                Users.create({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                }).then(user => {
                    res.status(201).json({
                        status: true,
                        message: `User with ID ${req.body.id} created`,
                        user
                    })
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
})




app.put('/users/:id', (req, res) =>{
    try{
        Users.findByPk(req.params.id).then(user => {
            if (user === null) {
                res.status(200).json({
                    status: false,
                    message: `User ID ${req.params.id} not found`,
                    user
                })
            } else {
                Users.update({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                }, {
                    where: {
                        id: req.params.id
                    }
                }).then(user => {
                    res.status(200).json({
                        status: true,
                        message: `User with ID ${req.params.id} updated!`,
                        user
                    })
                }) 
            }
        })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
})

app.delete('/users/:id', (req, res) => {
    try{
       Users.findOne({ where: {id: req.params.id}}).then(user => {
        if (user === null){
            res.status(201).json({
                status: false,
                message: `User ID ${req.params.id} does not exist`
            })
        } else {
            Users.destroy({ where: {id: req.params.id}}).then(user => {
                res.status(200).json({
                    status: true,
                    message: `User ID ${req.params.id} deleted`
                })
            })
        }
       })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
})
//start





app.listen(port, () => console.log(`Listening on port ${port}!`))
